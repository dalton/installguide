How you can contribute to this documentation
============================================

These pages are rendered using `RST/Sphinx <http://sphinx-doc.org/rest.html>`_ and served using
`Read the Docs <https://readthedocs.org>`_.
RST is a subset of Sphinx. Sphinx is RST with some extensions.


How to modify the webpages
--------------------------

The source code for this documentation is hosted on `GitLab <https://gitlab.com/dalton/installguide/>`_.
You need a GitLab account to modify the sources.

With a GitLab account you have two possibilities to edit the sources:

* If you are member of the dalton group, you can push
  directly to https://gitlab.com/dalton/installguide/. Once you commit and push, a webhook
  updates the documentation on http://dalton-installation.readthedocs.org/. This typically takes less than a minute.
* You fork https://gitlab.com/dalton/installguide/ and submit your changes at some point via a merge request. This means
  that your changes are not immediately visible but become so after a team member reviews your changes
  with a mouse click thus integrating them to https://gitlab.com/dalton/installguide/.

**Note that the entire documentation including the entire documentation
source code is public.**
Do not publish sensitive information and harvestable email addresses.


How to locally test changes
---------------------------

You do not have to push to see and test your changes.
You can test them locally.
For this install python-sphinx and python-matplotlib.
Then build the pages with::

  $ make html

Then point your browser to _build/html/index.html.
The style is not the same but the content is what you
would see after the git push.
