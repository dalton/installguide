Supported platforms and compilers
=================================

Many tests fail using Intel 2015. Therefore we currently do not recommend
compiling Dalton |version| using Intel 2015.
