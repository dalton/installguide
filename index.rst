Dalton/LSDalton |version| Installation Guide
============================================

.. toctree::
   :maxdepth: 2

   doc/supported.rst
   doc/general.rst
   doc/admins.rst
   doc/math.rst
   doc/scratch.rst
   doc/basis.rst
   doc/testing.rst
   doc/expert.rst
   doc/contributing.rst
